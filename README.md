# Bullfinch API - Python SDK

A complete example of contract onboarding user journey is at [setup_contract.py](https://gitlab.com/bullfinch-capital/public/api-client/python/-/blob/main/bullfinch/samples/setup_contract_v2.py#L70).
Even if Python is not your language of choice, this example shows to see the sequence of calls and the correct interaction with the API.

The REST API v2 is documented here: [redoc](https://api.platform.bullfinch.com/rest/v2/redoc) | [docs](https://api.platform.bullfinch.com/rest/v2/docs).

[![pipeline status](https://gitlab.com/bullfinch-capital/public/api-client/python/badges/main/pipeline.svg)](https://gitlab.com/bullfinch-capital/public/api-client/python/-/commits/main)

## Run samples

A developer sandbox is available at https://api.dev.bullfinch.com; matching authentication endpoint is at https://auth.dev.bullfinch.com/oauth2/token.

```
python3 -m venv venv && source venv/bin/activate
pip install git+https://gitlab.com/bullfinch-capital/public/api-client/python.git

CLIENT_ID=[your client id]
CLIENT_SECRET=[your client secret]

# Login and return the API token.
# -q for quiet; just prints the Bearer token.
python -m bullfinch.samples.login -q \
  --auth_endpoint=https://auth.dev.bullfinch.com/oauth2/token \
  --api_endpoint=https://api.dev.bullfinch.com \
  --client_id=${CLIENT_ID} \
  --secret=${CLIENT_SECRET}

# Create a contract in the Bullfinch REST API.
# -v for verbose; shows detailed interactions with the API.
# Product may be:
#   - remote flow: {bnpl, loan or mietkauf}.
#   - door to door flow: {bnpl_d2d, mietkau_d2d}.
# Country may be {DE, ES}.
python3 -m bullfinch.samples.setup_contract_v2 -v \
  --auth_endpoint=https://auth.dev.bullfinch.com/oauth2/token \
  --api_endpoint=https://api.dev.bullfinch.com \
  --client_id=${CLIENT_ID} \
  --secret=${CLIENT_SECRET} \
  --product=bnpl \
  --country=DE

deactivate
```

# Development

```
python3 -m venv venv && source venv/bin/activate
pip install -r test-requirements.txt
```

Now you're ready to develop.

## Generate updated client

Pay attention that discriminated fields will not work out of the box,
for example in `bullfinch/client_v2/apis/contract_hardware/hardware_create.py`.

```
rm -rf bullfinch/client bullfinch/client_v2 && \
  swagger_codegen generate https://api.test.bullfinch.com/rest/v2/openapi.json bullfinch/client_v2 && \
  isort --profile black . && black . && \
  grep -rl "= \.\.\."  bullfinch/client_v2/apis | xargs sed -i '' -e 's/= \.\.\./= None/g' && \
  black .
```
