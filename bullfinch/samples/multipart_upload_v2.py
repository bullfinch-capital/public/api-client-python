import logging
from typing import Optional

import requests
from tenacity import retry, stop_after_attempt, wait_fixed

from bullfinch.client_v2.apis.contract_documents.document_upload import (
    BodyDocumentUploadRestV2Contracts_contractId_documentsPost,
    DocumentRes,
)
from bullfinch.client_v2.apis.contract_documents.documents_list import DocumentsListRes


class MultipartUploadApi:
    def __init__(
        self,
        host: str,
        path: str,
        headers: Optional[dict[str, str]] = None,
    ):
        self.host = host
        self.path = path
        self.headers = headers

    def request_upload(
        self,
        body: BodyDocumentUploadRestV2Contracts_contractId_documentsPost,
        contract_id: str,
    ) -> DocumentRes:
        fields = body.dict()
        file_bytes = fields.pop("file_bytes")
        url = f"{self.host}{self.path}".format(contract_id=contract_id)

        @retry(stop=stop_after_attempt(30), wait=wait_fixed(1), reraise=True)
        def do_upload():
            r = requests.post(
                url,
                data=fields,
                files={"file_bytes": file_bytes},
                headers=self.headers,
            )
            logging.info("Response status: %s : %s", r.status_code, r.text)
            if r.status_code not in {200, 201}:
                logging.error("Response status: %s : %s", r.status_code, r.text)
                raise Exception(f"Response status: {r.status_code} : {r.text}")
            res_obj = DocumentsListRes.parse_obj(r.json())
            doc = res_obj.documents[0]
            return DocumentRes.parse_obj(doc)

        return do_upload()
