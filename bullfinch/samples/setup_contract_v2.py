import datetime
import logging
from time import sleep
from typing import Any, Optional

import requests
import tenacity
from requests.adapters import HTTPAdapter, Retry
from swagger_codegen.api.adapter.requests import RequestsAdapter
from swagger_codegen.api.configuration import Configuration

import bullfinch
import bullfinch.client_v2.apis.contract_comments.comment_create as comment_create
import bullfinch.client_v2.apis.customers.customer_create as customer_create
from bullfinch.client_v2.apis.contract_documents import document_upload
from bullfinch.client_v2.apis.contract_documents.get_customer_embedded_signing_url import (
    EmbeddedCustomerSignReq,
)
from bullfinch.client_v2.apis.contract_documents.start_embedded_signature_process import (
    EmbeddedEnvelopeSignersRes,
    EnvelopeSigner,
)
from bullfinch.client_v2.apis.contracts import contract_create
from bullfinch.client_v2.apis.contracts.contract_find import ContractFindReq
from bullfinch.client_v2.apis.contracts.contract_update import ContractPatchReq
from bullfinch.client_v2.apis.contracts.price_confirm import PriceConfirmReq
from bullfinch.client_v2.apis.contracts.submit_for_final_review import (
    AssetsPriceResult,
    FinalReviewRes,
)
from bullfinch.client_v2.apis.customers.customer_find import CustomerFindReq
from bullfinch.client_v2.apis.customers.customer_update import CustomerPatchReq
from bullfinch.client_v2.apis.installers.find_installer import (
    FindInstallerReq,
    InstallerRes,
)
from bullfinch.client_v2.apis.installers.list_installers import InstallersListRes
from bullfinch.client_v2.client import AutogeneratedApiClient as ApiV2Client
from bullfinch.client_v2.client import new_client
from bullfinch.samples import define_arguments
from bullfinch.samples.data_factory import DataFactory, Product
from bullfinch.samples.models import disc_hardware_bulk_create as hardware_bulk_create
from bullfinch.samples.models import disc_hardware_create as hardware_create
from bullfinch.samples.models import disc_hardware_get as hardware_get
from bullfinch.samples.models import disc_hardware_list as hardware_list
from bullfinch.samples.models import disc_hardware_update as hardware_update
from bullfinch.samples.multipart_upload_v2 import MultipartUploadApi

DocUploadPostReq = (
    document_upload.BodyDocumentUploadRestV2Contracts_contractId_documentsPost
)


def retry(func):
    return tenacity.retry(
        func,
        stop=tenacity.stop_after_attempt(60),
        wait=tenacity.wait_fixed(1),
        retry=tenacity.retry_if_not_exception_type(AssertionError),
        reraise=True,
    )


class ContractUserJourney:
    def __init__(
        self,
        bullfinch_client: ApiV2Client,
        factory: DataFactory,
        headers: Optional[dict[str, str]] = None,
        obo_cleantech_id: Optional[str] = None,
        verbose: bool = False,
    ):
        self.bfcl = bullfinch_client
        assert factory
        self.factory = factory
        self.headers = headers or {}
        self._duration_months: Optional[int] = None
        self._obo_cleantech_id = obo_cleantech_id
        self.verbose = verbose

    def journey(self):
        self.select_installer()

        # Contract creation
        customer_ids = self.create_customers()
        contract_id = self.create_contract(customer_ids)
        hardware_types_ids = self.create_hardware(contract_id)
        review_data = self.submit_for_final_review(contract_id)
        if review_data.computed_price_result.warnings:
            self.add_comment(
                contract_id,
                "The price is above limit for the following reasons (please provide them).",
            )
        self.confirm_price(contract_id, review_data.computed_price_result)

        if self.factory.product in {Product.bnpl_d2d, Product.mietkauf_d2d}:
            print("Door to Door Flow")

            # Generate documents and send to signature
            self.wait_for_contract_state(contract_id, "ready_for_signature")
            self.request_signature_documents_generation(contract_id)
            signers = self.request_signature_embedded(contract_id)
            signer: EnvelopeSigner
            for signer in signers.signers:
                sign_url = self.request_signature_URL(
                    contract_id,
                    "https://my.domain.com/page/to/redirect/after/signing/has/completed",
                    signer,
                )
                print(f"It's now the turn to sign of {signer.name} <{signer.email}>.")
                print(f"Please open in the browser the URL: {sign_url}.")
        else:
            print("Remote Flow")

            # Customer approval
            self.wait_for_contract_state(contract_id, "customer_approved")
            self.verify_credit_check_passed(contract_id)

            # Generate documents and send to signature
            self.request_signature_documents_generation(contract_id, wait=False)
            self.wait_for_contract_state(contract_id, "ready_for_signature")
            self.request_signature(contract_id)

        self.wait_for_contract_state(contract_id, "signed")

        # Complete installation.
        self.add_installed_hardware_info(contract_id, hardware_types_ids)
        self.add_asset_check_documents(contract_id, hardware_types_ids)
        self.add_system_connected_date(contract_id, hardware_types_ids)

        # Project complete
        self.wait_for_contract_state(contract_id, "project_complete")

    def set_obo_cleantech_id(self, cleantech_id: str):
        self._obo_cleantech_id = cleantech_id
        host = self.bfcl.configuration.host
        self.bfcl = get_client(
            host,
            headers=self.headers,
            params={"obo_cleantech_id": self._obo_cleantech_id},
            verbose=self.verbose,
        )

    @property
    def uploader(self) -> MultipartUploadApi:
        return MultipartUploadApi(
            self.bfcl.configuration.host,
            "/rest/v2/contracts/{contract_id}/documents",
            headers=self.headers,
        )

    @property
    def duration_months(self):
        if self._duration_months is not None:
            return self._duration_months
        products_result = self.bfcl.price.get_products()
        selected_product = None
        for product in products_result.products:
            if product.product == self.factory.product:
                selected_product = product
                self._duration_months = self.factory.random_choice(
                    product.tenors_months
                )
                break

        assert selected_product, "No product found"
        assert self._duration_months, f"valid values: {selected_product.tenors_months}"
        return self._duration_months

    @retry
    def list_installers(self) -> InstallersListRes:
        installers = self.bfcl.installers.list_installers()
        assert installers, "Invalid response when listing installers"
        return installers

    @retry
    def get_installer_id(self, installer: InstallerRes) -> str:
        cleantech_name = installer.data.external_id
        logging.info(
            "Getting installer cleantech with external id '%s'", cleantech_name
        )
        installer = self.bfcl.installers.find_installer(
            FindInstallerReq(external_id=cleantech_name)
        )
        assert (
            installer
        ), f"No installer cleantech found with external id '{cleantech_name}'"
        assert (
            installer.installer_id
        ), f"Installer cleantech with external id '{cleantech_name}' has no id"
        logging.info("Installer cleantech: %s", installer)
        return installer.installer_id

    def customer_post_req(self) -> customer_create.CustomerPostReq:
        """Create a customer."""
        f = self.factory
        p = f.person()
        c = customer_create.CustomerPostReq(
            first_name=p.first_name,
            last_name=p.last_name,
            gender=p.gender,
            date_of_birth=p.date_of_birth,
            e_mail=p.email.replace("@", f"+{f.numerify('############')}@"),
            tax_id=p.tax_id if f.country == "ES" else None,
            phone_number_struct=customer_create.PhoneNumber(
                international_prefix=f.international_prefix(),
                national_number=f.phone_number(),
            ),
            primary_address=self.factory.address(),
            invoice_address=self.factory.address(),
        )
        return c

    @retry
    def create_customers(self) -> list[str]:
        logging.info("Creating customers")
        c1 = self.bfcl.customers.customer_create(self.customer_post_req())
        c2 = self.bfcl.customers.customer_create(self.customer_post_req())
        cs = [c1, c2]
        logging.info(f"Created customers {c1.e_mail} and {c2.e_mail}")
        customer_ids = [c.customer_id for c in cs if c.customer_id]

        # Test customers find endpoint
        c = self.bfcl.customers.customer_find(CustomerFindReq(e_mail=c1.e_mail))

        # Test the customer update
        assert c.date_of_birth
        new_date_of_birth = c.date_of_birth
        while new_date_of_birth == c.date_of_birth:
            new_date_of_birth = self.factory.date_of_birth()
        _ = self.bfcl.customers.customer_update(
            CustomerPatchReq(date_of_birth=new_date_of_birth), c.customer_id
        )

        # Verify update and test customer get endpoint
        got_c = self.bfcl.customers.customer_get(c.customer_id)
        assert got_c.date_of_birth == new_date_of_birth
        return customer_ids

    def contract_post_req(
        self, customer_ids: list[str]
    ) -> contract_create.ContractPostReq:
        """Create a contract."""
        f = self.factory
        c = contract_create.ContractPostReq(
            customers=customer_ids,
            installation_address=self.factory.address(),
            financial=contract_create.Financial(
                financing_model_type=f.product,
                duration_months=self.duration_months,
            ),
            system=contract_create.System(
                heritage_protection=False,
                house_construction_year=datetime.date.today().year - 5,
                household_consumption_forecast_kwh=8,
                internet_connection=True,
                meter_cabinet=True,
                roof_maximum_tilt=8,
                roof_statics_checked=True,
            ),
            payment_data=contract_create.ContractPaymentData(
                bank="NATIONWIDE BUILDING SOCIETY",
                bic="MIDLGB22XXX",
                iban="GB16MIDL07009385995762",
            ),
        )
        assert c.system
        assert c.financial
        if f.product != Product.mietkauf:
            c.financial.downpayment = f.downpayment()
        return c

    @retry
    def create_contract(
        self,
        customer_ids: list[str],
    ) -> str:
        contract = self.bfcl.contracts.contract_create(
            self.contract_post_req(customer_ids),
        )
        assert contract.contract_id

        # Test the contract find endpoint
        contracts_list = self.bfcl.contracts.contract_find(
            ContractFindReq(customer_id=customer_ids[0])
        )
        assert len(contracts_list.contracts) == 1
        co = contracts_list.contracts.pop()

        # Test the update contract endpoint setting some extra metadata
        external_contract_id = self.factory.numerify("#########")
        _ = self.bfcl.contracts.contract_update(
            ContractPatchReq(
                external_contract_id=external_contract_id,
                installer_id=self._obo_cleantech_id,
            ),
            co.contract_id,
        )

        # Test the get contract endpoint
        got_co = self.bfcl.contracts.contract_get(co.contract_id)
        assert got_co.external_contract_id == external_contract_id

        return co.contract_id

    def req_create_pv(
        self,
    ) -> hardware_create.HardwarePostReq:
        f = self.factory
        return hardware_create.HardwarePostReq(
            manufacturer="Hanwha Q CELLS",
            model="Q.PEAK DUO-G8 blackframe (350Wp)",
            gross_price=f.pv_gross_price(),
            net_price=f.pv_net_price(),
            details=hardware_create.HardwareDetailsPV(
                type="solarPV",
                production_capacity_kwp=f.pv_production_capacity(),
                specific_yield_kwh_kwp=f.amount(min_value=3, max_value=6),
                number_of_modules=f.amount(8, 20),
            ),
        )

    def req_create_battery(
        self,
    ) -> hardware_create.HardwarePostReq:
        f = self.factory
        h = hardware_create.HardwarePostReq(
            manufacturer="BYD",
            model="HVS 10.2",
            gross_price=f.battery_gross_price(),
            net_price=f.battery_net_price(),
            details=hardware_create.HardwareDetailsBattery(
                type="batteryStorage",
                storage_capacity_kwh=f.battery_storage_capacity(),
            ),
        )
        return h

    def req_create_ev_charger(
        self,
    ) -> hardware_create.HardwarePostReq:
        f = self.factory
        return hardware_create.HardwarePostReq(
            manufacturer="EV Man",
            model="EV 1",
            gross_price=f.ev_charger_gross_price(),
            net_price=f.ev_charger_net_price(),
            details=hardware_create.HardwareDetailsEvCharger(
                type="EVCharger",
                power_kw=f.ev_charger_power(),
            ),
        )

    def req_create_inverter(self):
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsInverter(
                type="inverter",
                capacity_kw=10,
            ),
            manufacturer="Kostal",
            manufacturer_external_id="ext id",
            model="PIKO IQ 5.5",
        )

    def req_create_energy_management_system(self):
        f = self.factory
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsEnergyManagementSystem(
                type="energyManagementSystem",
            ),
            manufacturer="Brand",
            model="Model 22",
            net_price=f.net_price(180, 370),
        )

    def req_create_energy_meter(self):
        f = self.factory
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsEnergyMeter(
                type="energyMeter",
            ),
            manufacturer="Brand",
            model="Model 23",
            net_price=f.net_price(150, 400),
        )

    def req_create_meter_cabinet(self):
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsMeterCabinet(
                type="meterCabinet",
            ),
            manufacturer="Brand",
            model="Model 24",
        )

    def req_create_mounting_system(self):
        f = self.factory
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsMountingSystem(
                type="mountingSystem",
            ),
            manufacturer="mounting systems",
            model="all models",
            net_price=f.net_price(200, 500),
        )

    def req_create_optimizers(self):
        f = self.factory
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsOptimizers(
                type="optimizers",
                supported_pv_production_capacity_kw=f.optimizers_power_kw(),
            ),
            manufacturer="Optimus",
            model="Prime",
        )

    def req_create_special_scaffolding(self):
        return hardware_create.HardwarePostReq(
            details=hardware_create.HardwareDetailsSpecialScaffolding(
                type="specialScaffolding",
            ),
            manufacturer="Brand",
            model="Model 25",
        )

    # @retry  # Don't retry HW creation
    def create_hardware(self, contract_id: str) -> dict[str, str]:
        hw_combos = self.bfcl.price.get_hw_combos()
        assert hw_combos.hw_combinations, "No hardware combination obtained"
        hw_combos.hw_combinations = [
            c for c in hw_combos.hw_combinations if "solarPV" in c.hw_types
        ]
        assert (
            hw_combos.hw_combinations
        ), "No hardware combination including also a solarPV obtained"
        hw_combo = self.factory.random_choice(hw_combos.hw_combinations)
        hw_types = hw_combo.hw_types
        hw_types.extend(hw_combo.required_additional_hw)
        hw_types.extend(hw_combo.allowed_additional_hw)
        hw_creation_list: list[hardware_create.HardwarePostReq] = []
        for hw_type in hw_types:
            if hw_type in [hw.details.type for hw in hw_creation_list if hw.details]:
                continue
            if hw_type == "solarPV":
                hw_post_req = self.req_create_pv()
            elif hw_type == "batteryStorage":
                hw_post_req = self.req_create_battery()
            elif hw_type == "EVCharger":
                hw_post_req = self.req_create_ev_charger()
            elif hw_type == "inverter":
                hw_post_req = self.req_create_inverter()
            elif hw_type == "energyManagementSystem":
                hw_post_req = self.req_create_energy_management_system()
            elif hw_type == "energyMeter":
                hw_post_req = self.req_create_energy_meter()
            elif hw_type == "meterCabinet":
                hw_post_req = self.req_create_meter_cabinet()
            elif hw_type == "mountingSystem":
                hw_post_req = self.req_create_mounting_system()
            elif hw_type == "optimizers":
                hw_post_req = self.req_create_optimizers()
            elif hw_type == "specialScaffolding":
                hw_post_req = self.req_create_special_scaffolding()
            else:
                logging.warning("Not creating hardware of unmanaged type %s.", hw_type)
                continue
            hw_creation_list.append(hw_post_req)
        hardware_bulk_create_res = self.bfcl.contract_hardware.hardware_bulk_create(
            hardware_bulk_create.HardwareBulkPostReq(hardware=hw_creation_list),
            contract_id,
        )
        assert hardware_bulk_create_res.created_ids

        # Get contract hardware list and verify all items are there
        hardware_list_res = hardware_list.hardware_list(
            self.bfcl.contract_hardware, contract_id
        )
        hardware_items = hardware_list_res.hardware
        assert len(hardware_items) == len(hw_creation_list), hardware_items
        hardware_ids: dict[str, str] = {}
        for hw in hardware_items:
            # Test the get hardware endpoint
            custom_hardware = hardware_get.hardware_get(
                self.bfcl.contract_hardware, contract_id, hw.hardware_id
            )
            assert hw == custom_hardware, custom_hardware
            assert custom_hardware.details, custom_hardware.details

            hardware_ids[custom_hardware.details.type] = hw.hardware_id
        return hardware_ids

    @retry
    def submit_for_final_review(self, contract_id: str) -> FinalReviewRes:
        return self.bfcl.contracts.submit_for_final_review(contract_id)

    @retry
    def add_comment(self, contract_id: str, comment: str):
        return self.bfcl.contract_comments.comment_create(
            comment_create.CommentPostReq(comment=comment),
            contract_id,
        )

    @retry
    def confirm_price(self, contract_id: str, price: AssetsPriceResult):
        assert price.currency
        assert price.down_payment_cents is not None
        assert price.monthly_instalment and price.monthly_instalment.net_cents
        assert price.purchase_price and price.purchase_price.net_cents
        confirmed_price = PriceConfirmReq(
            currency=price.currency,
            down_payment_net_cents=price.down_payment_cents,
            monthly_instalment_net_cents=price.monthly_instalment.net_cents,
            purchase_price_net_cents=price.purchase_price.net_cents,
        )
        self.bfcl.contracts.price_confirm(confirmed_price, contract_id)

    @retry
    def request_signature_documents_generation(
        self, contract_id: str, wait: bool = True
    ):
        logging.info("Signature documents generation requested")
        self.bfcl.contract_documents.generate_documents_to_sign(
            contract_id,
            wait=wait,
        )

    @retry
    def request_signature(self, contract_id: str):
        self.bfcl.contract_documents.start_signature_process(
            contract_id,
        )
        logging.info("The signature process started")

    @retry
    def request_signature_embedded(
        self, contract_id: str
    ) -> EmbeddedEnvelopeSignersRes:
        signers = self.bfcl.contract_documents.start_embedded_signature_process(
            contract_id,
        )
        logging.info("The embedded signature process started")
        return signers

    @retry
    def request_signature_URL(
        self, contract_id: str, redirect_url: str, signer: EnvelopeSigner
    ):
        req = EmbeddedCustomerSignReq(return_url=redirect_url)
        resp = self.bfcl.contract_documents.get_customer_embedded_signing_url(
            req,
            contract_id,
            signer.customer_id,
        )
        return resp.signing_url

    @retry
    def verify_credit_check_passed(self, contract_id: str):
        credit_check_status = self.bfcl.contracts.credit_check_status(
            contract_id,
        )
        assert (
            credit_check_status.full_check
            and credit_check_status.full_check.status == "passed"
        )
        logging.info("Customers have passed the credit check")

    @retry
    def add_document(
        self,
        contract_id: str,
        content_type: str,
        doc_type: str,
        hardware_id: Optional[str],
        photo_subtype: Optional[str],
    ) -> str:
        document_res = self.uploader.request_upload(
            DocUploadPostReq(
                file_bytes=self.factory.image(image_format=content_type),
                types=[doc_type],
                hardware_ids=[hardware_id] if hardware_id else None,
                photo_subtype=photo_subtype,
            ),
            contract_id,
        )
        return document_res.document_id

    def add_pdf_document(
        self, contract_id: str, doc_type: str, hardware_id: Optional[str] = None
    ) -> str:
        return self.add_document(contract_id, "pdf", doc_type, hardware_id, None)

    def add_png_picture(
        self, contract_id: str, doc_type: str, hardware_id: Optional[str] = None
    ) -> str:
        return self.add_document(contract_id, "png", doc_type, hardware_id, None)

    def add_jpg_picture(
        self, contract_id: str, doc_type: str, photo_subtype: Optional[str] = None
    ) -> str:
        return self.add_document(contract_id, "jpeg", doc_type, None, photo_subtype)

    def cap_bnpl_document_upload_req(self) -> DocUploadPostReq:
        # Document which includes GDPR and TOS and CAP checks authorization.
        # Once these documents are attached to the contract the CAP checks will start.
        return DocUploadPostReq(
            file_bytes=self.factory.image(image_format="pdf"),
            types=[
                "annex_feed_in_tariff",
                "annex_gdpr",
                "annex_tos",
                "kyc_auth",
            ],
        )

    def add_asset_check_documents(
        self, contract_id: str, hardware_types_ids: dict[str, str]
    ):
        required_doc_types = {
            "installation_protocol",
            "installer_completion",
            "network_agency_registration",
            "mounting_system_statics",
            "technical_planning",
            "site_plan",
        }
        if self.factory.product == Product.loan:
            required_doc_types |= {"purchase_agreement_installer"}

        if self.factory.is_cl_es:
            required_doc_types |= {
                "roof_map",
                "application_public_registration",
            }

        uploaded_documents: dict[str, str] = {}
        for doc_type in required_doc_types:
            doc_id = self.add_pdf_document(contract_id, doc_type)
            uploaded_documents[doc_type] = doc_id
        logging.info(
            "Uploaded documents %s for contract %s.",
            uploaded_documents,
            contract_id,
        )

        # Take photos of the installation.
        photo_subtypes = ["pv_modules", "battery", "inverter", "meter_cabinet"]
        uploaded_documents = {}
        for photo_subtype in photo_subtypes:
            doc_id = self.add_jpg_picture(
                contract_id, "component_photo", photo_subtype=photo_subtype
            )
            uploaded_documents[photo_subtype] = doc_id

        logging.info(
            "Uploaded pictures for hardware %s for contract %s.",
            uploaded_documents,
            contract_id,
        )

        # Upload PV specific documents
        if pv_id := hardware_types_ids.get("solarPV"):
            uploaded_documents = {}
            for doc_type in {
                "string_plan",
            }:
                doc_id = self.add_png_picture(contract_id, doc_type, hardware_id=pv_id)
                uploaded_documents[doc_type] = doc_id
            for doc_type in {
                "circuit_diagram",
                "solar_modules_flash_tests",
            }:
                doc_id = self.add_pdf_document(contract_id, doc_type, hardware_id=pv_id)
                uploaded_documents[doc_type] = doc_id
            logging.info(
                "Uploaded PV specific documents %s for contract %s.",
                uploaded_documents,
                contract_id,
            )

    def add_cap_document(self, contract_id: str):
        if self.factory.product in {Product.bnpl, Product.mietkauf}:
            # Upload GDPR, TOS and CAP auth documents
            cap_document_data = self.cap_bnpl_document_upload_req()
            document = self.uploader.request_upload(cap_document_data, contract_id)

            assert set(document.types) == {
                "annex_feed_in_tariff",
                "annex_gdpr",
                "annex_tos",
                "kyc_auth",
            }

            # Download the document
            download_path = (
                "/rest/v2/contracts/{contract_id}/documents/{document_id}/download"
            )
            document_bytes = requests.get(
                self.bfcl.configuration.host
                + download_path.format(
                    contract_id=contract_id,
                    document_id=document.document_id,
                ),
                headers=self.headers,
            )

            assert document_bytes.content == cap_document_data.file_bytes

    def req_other_documents_upload(self) -> list[DocUploadPostReq]:
        document_upload_reqs: list[DocUploadPostReq] = list()

        # Required documents
        for doc_type in [
            "contract_transfer_customer",
            "hardware_transfer_invoice",
            "roof_map",
        ]:
            document_upload_reqs.append(
                DocUploadPostReq(
                    file_bytes=self.factory.image(image_format="pdf"),
                    types=[doc_type],
                )
            )
        for photo_doc_type in [
            "site_plan",
            "string_plan",
        ]:
            document_upload_reqs.append(
                DocUploadPostReq(
                    file_bytes=self.factory.image(image_format="jpeg"),
                    types=[photo_doc_type],
                )
            )
        return document_upload_reqs

    @retry
    def add_installed_hardware_info(
        self, contract_id: str, hardware_ids: dict[str, str]
    ):
        date_installation = datetime.date.today()
        date_operational = datetime.date.today()
        if "solarPV" in hardware_ids:
            _ = self.bfcl.contract_hardware.hardware_update(
                hardware_update.HardwarePatchReq(
                    details=hardware_update.HardwareDetailsPV(type="solarPV"),
                    timeline=hardware_update.Timeline(
                        installation=date_installation,
                        operational_readiness=date_operational,
                    ),
                    serial_number="pv0001",
                ),
                contract_id,
                hardware_ids["solarPV"],
            )
        if "batteryStorage" in hardware_ids:
            _ = self.bfcl.contract_hardware.hardware_update(
                hardware_update.HardwarePatchReq(
                    details=hardware_update.HardwareDetailsBattery(
                        type="batteryStorage"
                    ),
                    timeline=hardware_update.Timeline(
                        installation=date_installation,
                        operational_readiness=date_operational,
                    ),
                    serial_number="b00002",
                ),
                contract_id,
                hardware_ids["batteryStorage"],
            )
        if "inverter" in hardware_ids:
            _ = self.bfcl.contract_hardware.hardware_update(
                hardware_update.HardwarePatchReq(
                    details=hardware_update.HardwareDetailsInverter(type="inverter"),
                    serial_number="i00003",
                ),
                contract_id,
                hardware_ids["inverter"],
            )

    @retry
    def add_system_connected_date(self, contract_id: str, hardware_ids: dict[str, str]):
        if not hardware_ids.get("solarPV"):
            return
        _ = self.bfcl.contract_hardware.hardware_update(
            hardware_update.HardwarePatchReq(
                details=hardware_update.HardwareDetailsPV(
                    type="solarPV",
                    system_connected_date=datetime.date.today(),
                ),
            ),
            contract_id,
            hardware_ids["solarPV"],
        )

    def wait_for_contract_state(self, contract_id: str, contract_state: str) -> None:
        t0 = datetime.datetime.utcnow()
        for _ in range(1, 300):
            contract_res = self.bfcl.contracts.contract_get(contract_id)
            if contract_res and contract_res.state == contract_state:
                return
            sleep(5)
        elapsed = datetime.datetime.utcnow() - t0
        assert False, f"Contract failed to reach state {contract_state} in {elapsed}"

    @retry
    def select_installer(self):
        installers = self.list_installers()
        if installers.installers:
            installer = InstallerRes.parse_obj(
                self.factory.random_choice(installers.installers)
            )
            self.set_obo_cleantech_id(self.get_installer_id(installer))


def get_client(
    host: str,
    headers: dict[str, Any] = dict(),
    params: dict[str, Any] = dict(),
    verbose: bool = False,
) -> ApiV2Client:
    configuration = Configuration(host=host)
    session = requests.Session()
    retries = Retry(
        total=100, backoff_factor=0.2, status_forcelist=[500, 502, 503, 504]
    )
    session.mount("http://", HTTPAdapter(max_retries=retries))
    session.mount("https://", HTTPAdapter(max_retries=retries))
    session.headers.update(headers)
    session.params = params
    return new_client(
        adapter=RequestsAdapter(debug=verbose, session=session),
        configuration=configuration,
    )


def main():
    args = define_arguments()
    data_factory = DataFactory(
        args.country,
        args.product,
    )
    bearer_token = bullfinch.login(
        args.client_id,
        args.secret,
        args.auth_endpoint,
    )
    headers = {"Authorization": "Bearer " + bearer_token}

    client = get_client(args.api_endpoint, headers=headers, verbose=args.verbose)

    ContractUserJourney(
        client,
        data_factory,
        headers=headers,
        verbose=args.verbose,
    ).journey()


if __name__ == "__main__":
    main()
