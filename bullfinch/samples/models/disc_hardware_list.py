import typing

import pydantic
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest

from bullfinch.client_v2.apis.contract_hardware import (
    hardware_list as client_hardware_list,
)


class HardwareDetailsBattery(client_hardware_list.HardwareDetailsBattery):
    type: typing.Literal["batteryStorage"]


class HardwareDetailsEnergyManagementSystem(
    client_hardware_list.HardwareDetailsEnergyManagementSystem
):
    type: typing.Literal["energyManagementSystem"]


class HardwareDetailsEnergyMeter(client_hardware_list.HardwareDetailsEnergyMeter):
    type: typing.Literal["energyMeter"]


class HardwareDetailsEvCharger(client_hardware_list.HardwareDetailsEvCharger):
    type: typing.Literal["EVCharger"]


class HardwareDetailsHeatPump(client_hardware_list.HardwareDetailsHeatPump):
    type: typing.Literal["heatPump"]


class HardwareDetailsInverter(client_hardware_list.HardwareDetailsInverter):
    type: typing.Literal["inverter"]


class HardwareDetailsMeterCabinet(client_hardware_list.HardwareDetailsMeterCabinet):
    type: typing.Literal["meterCabinet"]


class HardwareDetailsMountingSystem(client_hardware_list.HardwareDetailsMountingSystem):
    type: typing.Literal["mountingSystem"]


class HardwareDetailsOptimizers(client_hardware_list.HardwareDetailsOptimizers):
    type: typing.Literal["optimizers"]


class HardwareDetailsPV(client_hardware_list.HardwareDetailsPV):
    type: typing.Literal["solarPV"]


class HardwareDetailsSpecialScaffolding(
    client_hardware_list.HardwareDetailsSpecialScaffolding
):
    type: typing.Literal["specialScaffolding"]


class HardwareRes(client_hardware_list.HardwareRes):
    details: typing.Optional[
        typing.Union[
            HardwareDetailsBattery,
            HardwareDetailsEnergyManagementSystem,
            HardwareDetailsEnergyMeter,
            HardwareDetailsEvCharger,
            HardwareDetailsHeatPump,
            HardwareDetailsInverter,
            HardwareDetailsMeterCabinet,
            HardwareDetailsMountingSystem,
            HardwareDetailsOptimizers,
            HardwareDetailsPV,
            HardwareDetailsSpecialScaffolding,
        ]
    ] = pydantic.Field(discriminator="type")


class HardwareListRes(client_hardware_list.HardwareListRes):
    hardware: typing.List[HardwareRes]  # type: ignore


def hardware_list(
    self: BaseApi,
    contract_id: str,
) -> HardwareListRes:
    """Hardware List"""

    body = None

    m = ApiRequest(
        method="GET",
        path="/rest/v2/contracts/{contract_id}/hardware".format(
            contract_id=contract_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({}),
        query_params=self._only_provided({}),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": HardwareListRes,
            },
            "400": {
                "application/json": client_hardware_list.ApiError,
            },
            "422": {
                "application/json": client_hardware_list.HTTPValidationError,
            },
            "500": {
                "application/json": client_hardware_list.ApiError,
            },
        },
        m,
    )
