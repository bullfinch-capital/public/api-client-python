import typing

import pydantic
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest

from bullfinch.client_v2.apis.contract_hardware import (
    hardware_get as client_hardware_get,
)
from bullfinch.client_v2.apis.contract_hardware.hardware_get import Timeline as Timeline


class HardwareDetailsBattery(client_hardware_get.HardwareDetailsBattery):
    type: typing.Literal["batteryStorage"]


class HardwareDetailsEnergyManagementSystem(
    client_hardware_get.HardwareDetailsEnergyManagementSystem
):
    type: typing.Literal["energyManagementSystem"]


class HardwareDetailsEnergyMeter(client_hardware_get.HardwareDetailsEnergyMeter):
    type: typing.Literal["energyMeter"]


class HardwareDetailsEvCharger(client_hardware_get.HardwareDetailsEvCharger):
    type: typing.Literal["EVCharger"]


class HardwareDetailsHeatPump(client_hardware_get.HardwareDetailsHeatPump):
    type: typing.Literal["heatPump"]


class HardwareDetailsInverter(client_hardware_get.HardwareDetailsInverter):
    type: typing.Literal["inverter"]


class HardwareDetailsMeterCabinet(client_hardware_get.HardwareDetailsMeterCabinet):
    type: typing.Literal["meterCabinet"]


class HardwareDetailsMountingSystem(client_hardware_get.HardwareDetailsMountingSystem):
    type: typing.Literal["mountingSystem"]


class HardwareDetailsOptimizers(client_hardware_get.HardwareDetailsOptimizers):
    type: typing.Literal["optimizers"]


class HardwareDetailsPV(client_hardware_get.HardwareDetailsPV):
    type: typing.Literal["solarPV"]


class HardwareDetailsSpecialScaffolding(
    client_hardware_get.HardwareDetailsSpecialScaffolding
):
    type: typing.Literal["specialScaffolding"]


class HardwareRes(client_hardware_get.HardwareRes):
    details: typing.Optional[
        typing.Union[
            HardwareDetailsBattery,
            HardwareDetailsEnergyManagementSystem,
            HardwareDetailsEnergyMeter,
            HardwareDetailsEvCharger,
            HardwareDetailsHeatPump,
            HardwareDetailsInverter,
            HardwareDetailsMeterCabinet,
            HardwareDetailsMountingSystem,
            HardwareDetailsOptimizers,
            HardwareDetailsPV,
            HardwareDetailsSpecialScaffolding,
        ]
    ] = pydantic.Field(discriminator="type")


def hardware_get(
    self: BaseApi,
    contract_id: str,
    hardware_id: str,
) -> HardwareRes:
    """Hardware Get"""

    body = None

    m = ApiRequest(
        method="GET",
        path="/rest/v2/contracts/{contract_id}/hardware/{hardware_id}".format(
            contract_id=contract_id,
            hardware_id=hardware_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({}),
        query_params=self._only_provided({}),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": HardwareRes,
            },
            "400": {
                "application/json": client_hardware_get.ApiError,
            },
            "404": {
                "default": None,
            },
            "422": {
                "application/json": client_hardware_get.HTTPValidationError,
            },
            "500": {
                "application/json": client_hardware_get.ApiError,
            },
        },
        m,
    )
