import argparse
import logging
import random
import textwrap

import coloredlogs

from bullfinch.samples.data_factory import Country, Product

PROD_URL_API = "https://api.platform.bullfinch.com"
PROD_URL_AUTH = "https://auth.platform.bullfinch.com/oauth2/token"

SANDOX_URL_API = "https://api.dev.bullfinch.com"
SANDOX_URL_AUTH = "https://auth.dev.bullfinch.com/oauth2/token"


__all__ = [
    "define_arguments",
]


_PRODUCTS = {
    Country.de: {Product.bnpl, Product.mietkauf},
    Country.es: {Product.loan},
}

random.seed()


def define_arguments():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            f"""\
            Sample Code to setup a contract using the Bullfinch API.

            Run against the Bullfinch API endpoint of your choice:
            - Production environment: {PROD_URL_API} | {PROD_URL_AUTH}
            - Development sandbox: {SANDOX_URL_API} | {SANDOX_URL_AUTH}
            """
        ),
    )
    parser.add_argument(
        "--api_endpoint",
        default=SANDOX_URL_API,
        help="API endpoint.",
    )
    parser.add_argument(
        "--auth_endpoint",
        default=SANDOX_URL_AUTH,
        help="Authentication endpoint.",
    )
    parser.add_argument("-c", "--client_id")
    parser.add_argument("-s", "--secret")

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase log verbosity.",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="Shows only the result.",
    )

    parser.add_argument(
        "--upload_kyc_auth",
        default=True,
        action=argparse.BooleanOptionalAction,
        dest="upload_kyc_auth",
        help="Upload kyc_auth document",
    )
    parser.add_argument(
        "--upload_documents",
        default=True,
        action=argparse.BooleanOptionalAction,
        dest="upload_documents",
        help="Upload a set of document types",
    )

    parser.add_argument(
        "--country",
        type=Country,
    )
    parser.add_argument(
        "--product",
        type=Product,
    )

    args = parser.parse_args()

    if args.verbose:
        coloredlogs.install(level="DEBUG")
    elif args.quiet:
        coloredlogs.install(level="ERROR")
    else:
        coloredlogs.install(level="INFO")

    if not args.country:
        args.country = random.choice(list(_PRODUCTS.keys()))
        logging.warning("picked random country: %s", args.country)
    if not args.product:
        args.product = random.choice(list(_PRODUCTS[args.country]))
        logging.warning("picked random product: %s", args.product)
    return args
