from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class CustomerFindReq(BaseModel):
    e_mail: typing.Optional[str] = None


class Money(BaseModel):
    currency: str
    value_cents: int


class Coordinates(BaseModel):
    latitude: typing.Optional[float] = None
    longitude: typing.Optional[float] = None


class ValidAddress(BaseModel):
    city: str
    country: str
    line1: str
    line2: typing.Optional[str] = None
    location: typing.Optional[Coordinates] = None
    postal_code: str


class PhoneNumber(BaseModel):
    international_prefix: typing.Optional[str] = None
    national_number: typing.Optional[str] = None


class CustomerRes(BaseModel):
    alternative_address: typing.Optional[ValidAddress] = None
    current_labor_contract_start_date: typing.Optional[datetime.date] = None
    customer_id: str
    date_of_birth: typing.Optional[datetime.date] = None
    e_mail: typing.Optional[str] = None
    first_name: typing.Optional[str] = None
    gender: typing.Optional[str] = None
    invoice_address: typing.Optional[ValidAddress] = None
    labor_situation: typing.Optional[str] = None
    last_name: typing.Optional[str] = None
    monthly_net_salary: typing.Optional[Money] = None
    outstanding_debts: typing.Optional[Money] = None
    outstanding_debts_types: typing.Optional[str] = None
    phone_number: typing.Optional[str] = None
    phone_number_struct: typing.Optional[PhoneNumber] = None
    place_of_birth: typing.Optional[str] = None
    previous_last_name: typing.Optional[str] = None
    primary_address: typing.Optional[ValidAddress] = None
    secondary_phone_number: typing.Optional[str] = None
    secondary_phone_number_struct: typing.Optional[PhoneNumber] = None
    tax_id: typing.Optional[str] = None
    tax_type: typing.Optional[str] = None


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: CustomerFindReq,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> CustomerRes:
    """Customer Find"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/customers/find".format(),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": CustomerRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
