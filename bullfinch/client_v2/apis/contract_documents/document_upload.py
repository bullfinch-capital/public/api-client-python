from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class BodyDocumentUploadRestV2Contracts_contractId_documentsPost(BaseModel):
    file_bytes: bytes
    photo_subtype: typing.Optional[str] = None
    types: typing.List[str]


class DocumentRes(BaseModel):
    document_id: str
    mime: typing.Optional[str] = None
    photo_subtype: typing.Optional[str] = None
    types: typing.List[str]


class DocumentsListRes(BaseModel):
    documents: typing.List[DocumentRes]


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: BodyDocumentUploadRestV2Contracts_contractId_documentsPost,
    contract_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> DocumentsListRes:
    """Document Upload"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/contracts/{contract_id}/documents".format(
            contract_id=contract_id,
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "201": {
                "application/json": DocumentsListRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
