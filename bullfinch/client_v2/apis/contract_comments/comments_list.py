from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class CommentRes(BaseModel):
    comment: typing.Optional[str] = None
    timestamp: typing.Optional[datetime.datetime] = None
    user: typing.Optional[str] = None


class CommentsListRes(BaseModel):
    comments: typing.List[CommentRes]


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    contract_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> CommentsListRes:
    """Comments List"""

    body = None

    m = ApiRequest(
        method="GET",
        path="/rest/v2/contracts/{contract_id}/comments".format(
            contract_id=contract_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": CommentsListRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
