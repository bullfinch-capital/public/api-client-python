from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import find_installer, get_installer, list_installers


class InstallersApi(BaseApi):
    list_installers = list_installers.make_request
    get_installer = get_installer.make_request
    find_installer = find_installer.make_request
