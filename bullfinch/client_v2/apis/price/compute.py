from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class AssetDetailsSpecialScaffolding(BaseModel):
    asset_type: str = "specialScaffolding"


class AssetDetailsPV(BaseModel):
    asset_type: str = "solarPV"
    production_capacity_wp: int


class AssetDetailsOptimizers(BaseModel):
    asset_type: str = "optimizers"
    supported_pv_production_capacity_w: int


class AssetDetailsMeterCabinet(BaseModel):
    asset_type: str = "meterCabinet"


class AssetDetailsHeatPump(BaseModel):
    asset_type: str = "heatPump"
    hp_type: typing.Optional[str] = None
    power_w: int


class AssetDetailsEvCharger(BaseModel):
    asset_type: str = "EVCharger"
    power_w: int


class AssetDetailsBattery(BaseModel):
    asset_type: str = "batteryStorage"
    storage_capacity_wh: int


class PurchasePrice(BaseModel):
    value_cents: int
    value_type: str


class AssetPriceInputDetails(BaseModel):
    details: typing.Union[
        AssetDetailsBattery,
        AssetDetailsEvCharger,
        AssetDetailsHeatPump,
        AssetDetailsMeterCabinet,
        AssetDetailsOptimizers,
        AssetDetailsPV,
        AssetDetailsSpecialScaffolding,
    ]
    purchase_price: PurchasePrice


class AssetsPriceInput(BaseModel):
    assets: typing.List[AssetPriceInputDetails]
    country: str
    downpayment_cents: typing.Optional[int] = None
    duration_months: int
    product: str
    request_id: typing.Optional[str] = None


class PriceLimitWarning(BaseModel):
    price_limit_cents: typing.Optional[int] = None


class PricingWarning(BaseModel):
    min_ticket_size_cents: typing.Optional[int] = None
    overpricing_hw: typing.Optional[typing.Dict[str, PriceLimitWarning]] = None
    price_sum: typing.Optional[int] = None


class PriceResult(BaseModel):
    gross_cents: int
    net_cents: int
    vat_cents: int


class PriceResultLine(BaseModel):
    component: typing.Optional[str] = None
    description: str
    gross_cents: int
    net_cents: int
    proportion_p100: float
    vat_cents: int
    vat_rate_p100: float


class AssetPriceResultDetails(BaseModel):
    asset_type: str
    invoice_lines: typing.List[PriceResultLine]


class AssetsPriceResult(BaseModel):
    annual_effective_interest_rate_micro: typing.Optional[int] = None
    annual_nominal_interest_rate_micro: typing.Optional[int] = None
    currency: str
    down_payment_cents: int
    instalment_per_asset: typing.List[AssetPriceResultDetails]
    monthly_instalment: PriceResult
    purchase_price: PriceResult
    purchase_price_per_asset: typing.List[AssetPriceResultDetails]
    warnings: typing.Optional[PricingWarning] = None


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: AssetsPriceInput,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> AssetsPriceResult:
    """Price Compute"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/price/compute".format(),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": AssetsPriceResult,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
