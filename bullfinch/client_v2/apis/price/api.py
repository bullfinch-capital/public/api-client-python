from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import compute, get_hw_combos, get_products


class PriceApi(BaseApi):
    compute = compute.make_request
    get_products = get_products.make_request
    get_hw_combos = get_hw_combos.make_request
