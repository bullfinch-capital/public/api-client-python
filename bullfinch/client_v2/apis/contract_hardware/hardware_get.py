from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class HardwareDetailsSpecialScaffolding(BaseModel):
    type: str = "specialScaffolding"


class PanelArrayData(BaseModel):
    azimut: typing.Optional[float] = None
    number_of_modules: typing.Optional[int] = None
    shading: typing.Optional[float] = None
    tilt: typing.Optional[float] = None


class Money(BaseModel):
    currency: str
    value_cents: int


class HardwareDetailsPV(BaseModel):
    agreed_meter_installation_date: typing.Optional[datetime.date] = None
    arrays: typing.Optional[typing.List[PanelArrayData]] = None
    feed_in_tariff: typing.Optional[Money] = None
    module_capacity_wp: typing.Optional[int] = None
    number_of_modules: typing.Optional[int] = None
    production_capacity_kwp: typing.Optional[float] = None
    single_module_price: typing.Optional[Money] = None
    specific_yield_kwh_kwp: typing.Optional[float] = None
    system_connected_date: typing.Optional[datetime.date] = None
    type: str = "solarPV"


class HardwareDetailsOptimizers(BaseModel):
    supported_pv_production_capacity_kw: typing.Optional[float] = None
    type: str = "optimizers"


class HardwareDetailsMountingSystem(BaseModel):
    type: str = "mountingSystem"


class HardwareDetailsMeterCabinet(BaseModel):
    type: str = "meterCabinet"


class HardwareDetailsInverter(BaseModel):
    capacity_kw: typing.Optional[float] = None
    inverter_type: typing.Optional[str] = None
    type: str = "inverter"


class HardwareDetailsHeatPump(BaseModel):
    capacity_kw: typing.Optional[float] = None
    hp_type: typing.Optional[str] = None
    type: str = "heatPump"


class HardwareDetailsEvCharger(BaseModel):
    power_kw: typing.Optional[float] = None
    type: str = "EVCharger"


class HardwareDetailsEnergyMeter(BaseModel):
    type: str = "energyMeter"


class HardwareDetailsEnergyManagementSystem(BaseModel):
    type: str = "energyManagementSystem"


class HardwareDetailsBattery(BaseModel):
    storage_capacity_kwh: typing.Optional[float] = None
    type: str = "batteryStorage"


class Timeline(BaseModel):
    commissioning_protocol: typing.Optional[datetime.date] = None
    hardware_end: typing.Optional[datetime.date] = None
    hardware_transfer: typing.Optional[datetime.date] = None
    installation: typing.Optional[datetime.date] = None
    installer_order: typing.Optional[datetime.date] = None
    operational_readiness: typing.Optional[datetime.date] = None
    warranty_end: typing.Optional[datetime.date] = None


class HardwareRes(BaseModel):
    details: typing.Optional[
        typing.Union[
            HardwareDetailsBattery,
            HardwareDetailsEnergyManagementSystem,
            HardwareDetailsEnergyMeter,
            HardwareDetailsEvCharger,
            HardwareDetailsHeatPump,
            HardwareDetailsInverter,
            HardwareDetailsMeterCabinet,
            HardwareDetailsMountingSystem,
            HardwareDetailsOptimizers,
            HardwareDetailsPV,
            HardwareDetailsSpecialScaffolding,
        ]
    ] = None
    gross_price: typing.Optional[Money] = None
    hardware_id: str
    manufacturer: typing.Optional[str] = None
    manufacturer_external_id: typing.Optional[str] = None
    model: typing.Optional[str] = None
    net_price: typing.Optional[Money] = None
    serial_number: typing.Optional[str] = None
    status: typing.Optional[str] = None
    timeline: typing.Optional[Timeline] = None
    warranty_years: typing.Optional[int] = None


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    contract_id: str,
    hardware_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> HardwareRes:
    """Hardware Get"""

    body = None

    m = ApiRequest(
        method="GET",
        path="/rest/v2/contracts/{contract_id}/hardware/{hardware_id}".format(
            contract_id=contract_id,
            hardware_id=hardware_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": HardwareRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "404": {
                "default": None,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
