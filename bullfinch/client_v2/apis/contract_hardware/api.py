from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import (
    hardware_bulk_create,
    hardware_create,
    hardware_get,
    hardware_list,
    hardware_update,
)


class ContractHardwareApi(BaseApi):
    hardware_list = hardware_list.make_request
    hardware_create = hardware_create.make_request
    hardware_bulk_create = hardware_bulk_create.make_request
    hardware_get = hardware_get.make_request
    hardware_update = hardware_update.make_request
