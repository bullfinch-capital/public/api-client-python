from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import (
    contract_create,
    contract_find,
    contract_get,
    contract_update,
    credit_check_status,
    price_confirm,
    submit_for_final_review,
)


class ContractsApi(BaseApi):
    contract_find = contract_find.make_request
    contract_create = contract_create.make_request
    contract_get = contract_get.make_request
    contract_update = contract_update.make_request
    credit_check_status = credit_check_status.make_request
    price_confirm = price_confirm.make_request
    submit_for_final_review = submit_for_final_review.make_request
