from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class ContractTimeline(BaseModel):
    countersigned_date: typing.Optional[datetime.date] = None
    customer_signed_earliest: typing.Optional[datetime.date] = None
    customer_signed_latest: typing.Optional[datetime.date] = None
    grid_operator_application: typing.Optional[datetime.date] = None
    hardware_transfer: typing.Optional[datetime.date] = None
    legal_end: typing.Optional[datetime.date] = None
    legal_start: typing.Optional[datetime.date] = None
    withdrawal: typing.Optional[datetime.date] = None
    withdrawal_period_end: typing.Optional[datetime.date] = None


class ContractPaymentData(BaseModel):
    bank: typing.Optional[str] = None
    bic: typing.Optional[str] = None
    iban: typing.Optional[str] = None


class CustomerProportion(BaseModel):
    customer_id: typing.Optional[str] = None
    proportion: typing.Optional[str] = None


class Subsidy(BaseModel):
    type: typing.Optional[str] = None


class Money(BaseModel):
    currency: str
    value_cents: int


class Financial(BaseModel):
    capex_as_designed: typing.Optional[Money] = None
    customers_purchase_proportion: typing.Optional[
        typing.List[CustomerProportion]
    ] = None
    downpayment: typing.Optional[Money] = None
    duration_months: typing.Optional[int] = None
    financing_model_type: typing.Optional[str] = None
    financing_payment_type: typing.Optional[str] = None
    fixed_amount_components: typing.Optional[typing.List[str]] = None
    subsidies: typing.Optional[typing.List[Subsidy]] = None


class System(BaseModel):
    expected_annual_degradation_p_100: typing.Optional[float] = None
    grid_operator: typing.Optional[str] = None
    guaranteed_production_kwh: typing.Optional[int] = None
    guaranteed_solar_rate_per_kwh: typing.Optional[Money] = None
    heritage_protection: typing.Optional[bool] = None
    house_construction_year: typing.Optional[int] = None
    household_consumption_forecast_kwh: typing.Optional[float] = None
    installation_company: typing.Optional[str] = None
    internet_connection: typing.Optional[bool] = None
    meter_cabinet: typing.Optional[bool] = None
    rent_factor: typing.Optional[float] = None
    replanned: typing.Optional[bool] = None
    residue_value_10_years: typing.Optional[Money] = None
    roof_maximum_tilt: typing.Optional[int] = None
    roof_statics_checked: typing.Optional[bool] = None
    self_consumption_p100: typing.Optional[float] = None
    utility_company: typing.Optional[str] = None


class Coordinates(BaseModel):
    latitude: typing.Optional[float] = None
    longitude: typing.Optional[float] = None


class ValidAddress(BaseModel):
    city: str
    country: str
    line1: str
    line2: typing.Optional[str] = None
    location: typing.Optional[Coordinates] = None
    postal_code: str


class ContractPostReq(BaseModel):
    contact_customer_id: typing.Optional[str] = None
    customers: typing.List[str]
    electricity_yearly_expense_amount: typing.Optional[Money] = None
    external_contract_id: typing.Optional[str] = None
    external_status: typing.Optional[str] = None
    financial: typing.Optional[Financial] = None
    house_inhabitants: typing.Optional[int] = None
    installation_address: ValidAddress
    payment_data: typing.Optional[ContractPaymentData] = None
    portfolio: typing.Optional[str] = None
    system: typing.Optional[System] = None
    timeline: typing.Optional[ContractTimeline] = None


class Address(BaseModel):
    city: typing.Optional[str] = None
    country: typing.Optional[str] = None
    line1: typing.Optional[str] = None
    line2: typing.Optional[str] = None
    location: typing.Optional[Coordinates] = None
    postal_code: typing.Optional[str] = None


class ContractRes(BaseModel):
    contact_customer_id: typing.Optional[str] = None
    contract_id: str
    customers: typing.Optional[typing.List[str]] = None
    electricity_yearly_expense_amount: typing.Optional[Money] = None
    external_contract_id: typing.Optional[str] = None
    external_status: typing.Optional[str] = None
    financial: typing.Optional[Financial] = None
    house_inhabitants: typing.Optional[int] = None
    installation_address: typing.Optional[Address] = None
    payment_data: typing.Optional[ContractPaymentData] = None
    portfolio: typing.Optional[str] = None
    short_id: typing.Optional[str] = None
    state: str
    system: typing.Optional[System] = None
    timeline: typing.Optional[ContractTimeline] = None


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    __request__: ContractPostReq,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> ContractRes:
    """Contract Create"""

    body = __request__

    m = ApiRequest(
        method="POST",
        path="/rest/v2/contracts".format(),
        content_type="application/json",
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "201": {
                "application/json": ContractRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
