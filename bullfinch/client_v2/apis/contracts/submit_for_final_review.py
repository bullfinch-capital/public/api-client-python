from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class PriceLimitWarning(BaseModel):
    price_limit_cents: typing.Optional[int] = None


class PricingWarning(BaseModel):
    min_ticket_size_cents: typing.Optional[int] = None
    overpricing_hw: typing.Optional[typing.Dict[str, PriceLimitWarning]] = None
    price_sum: typing.Optional[int] = None


class PriceResult(BaseModel):
    gross_cents: int
    net_cents: int
    vat_cents: int


class PriceResultLine(BaseModel):
    component: typing.Optional[str] = None
    description: str
    gross_cents: int
    net_cents: int
    proportion_p100: float
    vat_cents: int
    vat_rate_p100: float


class AssetPriceResultDetails(BaseModel):
    asset_type: str
    invoice_lines: typing.List[PriceResultLine]


class AssetsPriceResult(BaseModel):
    annual_effective_interest_rate_micro: typing.Optional[int] = None
    annual_nominal_interest_rate_micro: typing.Optional[int] = None
    currency: str
    down_payment_cents: int
    instalment_per_asset: typing.List[AssetPriceResultDetails]
    monthly_instalment: PriceResult
    purchase_price: PriceResult
    purchase_price_per_asset: typing.List[AssetPriceResultDetails]
    warnings: typing.Optional[PricingWarning] = None


class FinalReviewRes(BaseModel):
    computed_price_result: AssetsPriceResult


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    contract_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> FinalReviewRes:
    """Submit For Final Review"""

    body = None

    m = ApiRequest(
        method="POST",
        path="/rest/v2/contracts/{contract_id}/submit_for_final_review".format(
            contract_id=contract_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": FinalReviewRes,
            },
            "400": {
                "application/json": ApiError,
            },
            "404": {
                "default": None,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
