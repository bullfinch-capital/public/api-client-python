from __future__ import annotations

import datetime
import typing

import pydantic
from pydantic import BaseModel
from swagger_codegen.api import json
from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest


class FullCheck(BaseModel):
    date: typing.Optional[datetime.datetime] = None
    status: str


class Money(BaseModel):
    currency: str
    value_cents: int


class PreCheck(BaseModel):
    date: typing.Optional[datetime.datetime] = None
    max_capex: typing.Optional[Money] = None
    status: str
    validity_months: typing.Optional[int] = None


class CreditCheckStatus(BaseModel):
    full_check: FullCheck
    pre_check: PreCheck


class ApiError(BaseModel):
    detail: str
    translated_msg: typing.Optional[typing.Dict[str, str]] = None


class ValidationError(BaseModel):
    loc: typing.List[str]
    msg: str
    type: str


class HTTPValidationError(BaseModel):
    detail: typing.Optional[typing.List[ValidationError]] = None


def make_request(
    self: BaseApi,
    contract_id: str,
    id_token: str = None,
    accept_language: str = None,
    obo_cleantech_id: str = None,
) -> CreditCheckStatus:
    """Get Creditcheck Status"""

    body = None

    m = ApiRequest(
        method="GET",
        path="/rest/v2/contracts/{contract_id}/credit_check".format(
            contract_id=contract_id,
        ),
        content_type=None,
        body=body,
        headers=self._only_provided(
            {
                "id_token": id_token,
                "accept-language": accept_language,
            }
        ),
        query_params=self._only_provided(
            {
                "obo_cleantech_id": obo_cleantech_id,
            }
        ),
        cookies=self._only_provided({}),
    )
    return self.make_request(
        {
            "200": {
                "application/json": CreditCheckStatus,
            },
            "400": {
                "application/json": ApiError,
            },
            "404": {
                "default": None,
            },
            "422": {
                "application/json": HTTPValidationError,
            },
            "500": {
                "application/json": ApiError,
            },
        },
        m,
    )
