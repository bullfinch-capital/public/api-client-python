-r requirements.txt

black==21.12.b0
isort==5.10.1
mypy==0.942
pytest==6.2.5
tox==3.24.5
wheel==0.45.1
